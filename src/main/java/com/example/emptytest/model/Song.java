package com.example.emptytest.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Song {
    private Long id;
    private String title;
    private String artist;
}
