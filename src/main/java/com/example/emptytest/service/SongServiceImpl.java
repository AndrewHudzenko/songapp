package com.example.emptytest.service;

import com.example.emptytest.model.Song;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongServiceImpl implements SongService {
    private List<Song> songs;

    public SongServiceImpl() {
        songs = List.of(
                new Song(1L, "Song 1", "Artist 1"),
                new Song(2L, "Song 2", "Artist 2"),
                new Song(3L, "Song 3", "Artist 3")
        );
    }

    @Override
    public List<Song> getSongs() {
        return songs;
    }
}
