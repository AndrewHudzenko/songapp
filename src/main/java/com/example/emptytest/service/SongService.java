package com.example.emptytest.service;

import com.example.emptytest.model.Song;
import org.springframework.stereotype.Service;

import java.util.List;

public interface SongService {
    List<Song> getSongs();
}
